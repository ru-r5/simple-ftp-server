FROM python:3.9-slim-bullseye

ENV DEBIAN_FRONTEND noninteractive

ENV FTP_PORT=21 
ENV FTP_PASS=ftp 
ENV FTP_USER=ftp 
ENV FTP_ROOT=/ftp-home 

RUN apt-get update -yqq && \
    apt-get install -yqq --no-install-recommends \
    make \
    gcc \
    g++ \
    apt-utils \
    apt-transport-https \
    ca-certificates \
    build-essential \
    libaio-dev \
    gnupg2 \
    curl \
    nano

RUN pip install -U pip
RUN pip install setuptools wheel
RUN pip install pyftpdlib

RUN apt-get purge --auto-remove -yqq make gcc g++ && \
    apt-get autoremove -yqq --purge && \
    apt-get clean && \
    rm -rf \
        /var/lib/apt/lists/* \
        /tmp/* \
        /var/tmp/* \
        /usr/share/man \
        /usr/share/doc \
        /usr/share/doc-base

ENV DEBIAN_FRONTEND teletype

COPY simple-ftp-server /bin/simple-ftp-server

RUN mkdir /ftp-home

EXPOSE ${FTP_PORT}/tcp

ENTRYPOINT python /bin/simple-ftp-server 