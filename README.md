# simple-ftp-server docker service based on https://github.com/giampaolo/pyftpdlib  
Example:
```
docker build -t simple-ftp-server .
docker build -f Dockerfile_client -t ftp-client .
docker run -id --rm --name ftp-server --network="host" simple-ftp-server
docker run -it --rm --network="host" ftp-client bash
python test.py
docker stop ftp-server
```
