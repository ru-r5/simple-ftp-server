import ftputil

session_factory = ftputil.session.session_factory(
                       #base_class=ftplib.FTP,
                       port=21,
                       #encoding='UTF-8',
                       #encrypt_data_channel=False,
                       #use_passive_mode=False,
                       debug_level=2)

with ftputil.FTPHost('0.0.0.0', 'ftp', 'ftp',
                     session_factory=session_factory
                     ) as ftp:
    ftp.chdir('/')
    recursive = ftp.listdir('./')

    for files in recursive:
        print(f'{files}')
    print('-' * 80)
